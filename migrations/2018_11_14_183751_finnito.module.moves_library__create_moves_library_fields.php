<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;
use Finnito\MovesLibraryModule\Style\StyleModel;
use Finnito\MovesLibraryModule\Type\TypeModel;

class FinnitoModuleMovesLibraryCreateMovesLibraryFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        /* Shared Fields */
        'name' => 'anomaly.field_type.text',
        'slug' => [
            'type' => 'anomaly.field_type.slug',
            'config' => [
                'slugify' => 'name',
                'type' => '-'
            ],
        ],

        /* Move Fields */
        "style" => [
            "type" => "anomaly.field_type.relationship",
            "config" => [
                "related" => StyleModel::class,
            ],
        ],
        "level" => [
            "type" => "anomaly.field_type.relationship",
            "config" => [
                "related" => TypeModel::class,
            ],
        ],
        "video" => [
            "type" => "anomaly.field_type.url",
            "config" => [
                "default_value" => "https://youtube.com",
            ],
        ],
        "counts" => [
            "type" => "anomaly.field_type.integer",
            "config" => [
                "separator" => "",
            ],
        ],
        "description" => [
            "type" => "anomaly.field_type.wysiwyg",
            "config" => [
                "default_value" => "
                    <h3>Breakdown</h3>
                    <ol>
                        <li>Steps for count 1..</li>
                    </ol>
                    <h3>Tips & Tricks</h3>
                    <p></p>",
            ],
        ],
    ];

}
