<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FinnitoModuleMovesLibraryCreateTypeStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'type',
         'title_column' => 'name',
         'translatable' => false,
         'versionable' => true,
         'trashable' => false,
         'searchable' => true,
         'sortable' => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'name' => [
            'required' => true,
        ],
        'slug' => [
            'unique' => true,
            'required' => true,
        ],
    ];

}
