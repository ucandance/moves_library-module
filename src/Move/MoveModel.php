<?php namespace Finnito\MovesLibraryModule\Move;

use Finnito\MovesLibraryModule\Move\Contract\MoveInterface;
use Anomaly\Streams\Platform\Model\MovesLibrary\MovesLibraryMovesEntryModel;

class MoveModel extends MovesLibraryMovesEntryModel implements MoveInterface
{
    public function youtubeThumbnailURL()
    {
        $id = explode("?v=", $this->video)[1];
        $str = "https://img.youtube.com/vi/{$id}/mqdefault.jpg";
        return $str;
    }
}
