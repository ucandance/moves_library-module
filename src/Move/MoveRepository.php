<?php namespace Finnito\MovesLibraryModule\Move;

use Finnito\MovesLibraryModule\Move\Contract\MoveRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class MoveRepository extends EntryRepository implements MoveRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var MoveModel
     */
    protected $model;

    /**
     * Create a new MoveRepository instance.
     *
     * @param MoveModel $model
     */
    public function __construct(MoveModel $model)
    {
        $this->model = $model;
    }

    public function counts()
    {
        return $this
            ->model
            ->all()
            ->sortBy("counts")
            ->unique("counts")
            ->pluck("counts");
    }
}
