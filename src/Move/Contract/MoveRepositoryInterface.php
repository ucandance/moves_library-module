<?php namespace Finnito\MovesLibraryModule\Move\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface MoveRepositoryInterface extends EntryRepositoryInterface
{
    public function counts();
}
