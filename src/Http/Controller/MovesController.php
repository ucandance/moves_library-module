<?php namespace Finnito\MovesLibraryModule\Http\Controller;

use Finnito\MovesLibraryModule\Style\Contract\StyleRepositoryInterface;
use Finnito\MovesLibraryModule\Move\Contract\MoveRepositoryInterface;
use Finnito\MovesLibraryModule\Type\Contract\TypeRepositoryInterface;
use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Illuminate\Contracts\Auth\Guard;

/**
 * Class MovesController
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class MovesController extends PublicController
{
    /**
     * Return an index of all moves.
     *
     * @return \Illuminate\Contracts\View\View|mixed
     */
    public function index(
        Guard $auth,
        MoveRepositoryInterface $moves,
        StyleRepositoryInterface $styles,
        TypeRepositoryInterface $levels
    ) {
        // Check if logged in
        if (!$user = $auth->user()) {
            abort(403);
        }

        // Check if has permissions
        if (!$user->hasAnyRole(["admin", "committee", "volunteer"])) {
            abort(403);
        }

        // Sufficient permissions, so continue.
        // Breadcrumbs
        $this->breadcrumbs->add("Home", "/");
        $this->breadcrumbs->add('Moves Library', '/moves-library');

        // Meta Information
        $this->template->set("meta_title", "Moves Library");

        $title = "Moves Library";
        return $this->view->make(
            "finnito.module.moves_library::index",
            [
                "title" => $title,
                "moves" => $moves->all(),
                "styles" => $styles->all(),
                "levels" => $levels->all(),
                "counts" => $moves->counts(),
            ]
        );
    }

    /**
     * Return view of a single move.
     *
     * @param MoveRepositoryInterface $moves
     * @param $slug
     * @return \Illuminate\Contracts\View\View|mixed
     */
    public function single(Guard $auth, MoveRepositoryInterface $moves, $slug)
    {
        // Check if logged in
        if (!$user = $auth->user()) {
            abort(403);
        }

        // Check if has permissions
        if (!$user->hasAnyRole(["admin", "committee", "volunteer"])) {
            abort(403);
        }

        // Check if the move exists
        if (!$move = $moves->findBy("slug", $slug)) {
            abort(404);
        }

        // Add breadcrumbs
        $this->breadcrumbs->add("Home", "/");
        $this->breadcrumbs->add('Moves Library', '/moves-library');
        $this->breadcrumbs->add($move->name, $this->request->path());
        $this->template->set("meta_title", $move->name);
        $this->template->set('edit_link', "/admin/moves_library/edit/".$move->id);
        $this->template->set("edit_type", $move->name);

        // Render
        return $this->view->make(
            "finnito.module.moves_library::single",
            [
                "move" => $move,
            ]
        );
    }
}
