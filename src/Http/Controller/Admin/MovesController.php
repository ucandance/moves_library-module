<?php namespace Finnito\MovesLibraryModule\Http\Controller\Admin;

use Finnito\MovesLibraryModule\Move\Form\MoveFormBuilder;
use Finnito\MovesLibraryModule\Move\Table\MoveTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class MovesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param MoveTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(MoveTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param MoveFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(MoveFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param MoveFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(MoveFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
