<?php namespace Finnito\MovesLibraryModule\Http\Controller\Admin;

use Finnito\MovesLibraryModule\Style\Form\StyleFormBuilder;
use Finnito\MovesLibraryModule\Style\Table\StyleTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class StylesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param StyleTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(StyleTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param StyleFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(StyleFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param StyleFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(StyleFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
