<?php namespace Finnito\MovesLibraryModule;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Finnito\MovesLibraryModule\Type\Contract\TypeRepositoryInterface;
use Finnito\MovesLibraryModule\Type\TypeRepository;
use Anomaly\Streams\Platform\Model\MovesLibrary\MovesLibraryTypeEntryModel;
use Finnito\MovesLibraryModule\Type\TypeModel;
use Finnito\MovesLibraryModule\Style\Contract\StyleRepositoryInterface;
use Finnito\MovesLibraryModule\Style\StyleRepository;
use Anomaly\Streams\Platform\Model\MovesLibrary\MovesLibraryStylesEntryModel;
use Finnito\MovesLibraryModule\Style\StyleModel;
use Finnito\MovesLibraryModule\Move\Contract\MoveRepositoryInterface;
use Finnito\MovesLibraryModule\Move\MoveRepository;
use Anomaly\Streams\Platform\Model\MovesLibrary\MovesLibraryMovesEntryModel;
use Finnito\MovesLibraryModule\Move\MoveModel;
use Illuminate\Routing\Router;

class MovesLibraryModuleServiceProvider extends AddonServiceProvider
{

    /**
     * Additional addon plugins.
     *
     * @type array|null
     */
    protected $plugins = [];

    /**
     * The addon Artisan commands.
     *
     * @type array|null
     */
    protected $commands = [];

    /**
     * The addon's scheduled commands.
     *
     * @type array|null
     */
    protected $schedules = [];

    /**
     * The addon API routes.
     *
     * @type array|null
     */
    protected $api = [];

    /**
     * The addon routes.
     *
     * @type array|null
     */
    protected $routes = [
        'admin/moves_library/type'           => 'Finnito\MovesLibraryModule\Http\Controller\Admin\TypeController@index',
        'admin/moves_library/type/create'    => 'Finnito\MovesLibraryModule\Http\Controller\Admin\TypeController@create',
        'admin/moves_library/type/edit/{id}' => 'Finnito\MovesLibraryModule\Http\Controller\Admin\TypeController@edit',
        'admin/moves_library/styles'           => 'Finnito\MovesLibraryModule\Http\Controller\Admin\StylesController@index',
        'admin/moves_library/styles/create'    => 'Finnito\MovesLibraryModule\Http\Controller\Admin\StylesController@create',
        'admin/moves_library/styles/edit/{id}' => 'Finnito\MovesLibraryModule\Http\Controller\Admin\StylesController@edit',
        'admin/moves_library'           => 'Finnito\MovesLibraryModule\Http\Controller\Admin\MovesController@index',
        'admin/moves_library/create'    => 'Finnito\MovesLibraryModule\Http\Controller\Admin\MovesController@create',
        'admin/moves_library/edit/{id}' => 'Finnito\MovesLibraryModule\Http\Controller\Admin\MovesController@edit',

        "moves-library" => "Finnito\MovesLibraryModule\Http\Controller\MovesController@index",
        "moves-library/{slug}" => "Finnito\MovesLibraryModule\Http\Controller\MovesController@single",
    ];

    /**
     * The addon middleware.
     *
     * @type array|null
     */
    protected $middleware = [
        //Finnito\MovesLibraryModule\Http\Middleware\ExampleMiddleware::class
    ];

    /**
     * Addon group middleware.
     *
     * @var array
     */
    protected $groupMiddleware = [
        //'web' => [
        //    Finnito\MovesLibraryModule\Http\Middleware\ExampleMiddleware::class,
        //],
    ];

    /**
     * Addon route middleware.
     *
     * @type array|null
     */
    protected $routeMiddleware = [];

    /**
     * The addon event listeners.
     *
     * @type array|null
     */
    protected $listeners = [
        //Finnito\MovesLibraryModule\Event\ExampleEvent::class => [
        //    Finnito\MovesLibraryModule\Listener\ExampleListener::class,
        //],
    ];

    /**
     * The addon alias bindings.
     *
     * @type array|null
     */
    protected $aliases = [
        //'Example' => Finnito\MovesLibraryModule\Example::class
    ];

    /**
     * The addon class bindings.
     *
     * @type array|null
     */
    protected $bindings = [
        MovesLibraryTypeEntryModel::class => TypeModel::class,
        MovesLibraryStylesEntryModel::class => StyleModel::class,
        MovesLibraryMovesEntryModel::class => MoveModel::class,
    ];

    /**
     * The addon singleton bindings.
     *
     * @type array|null
     */
    protected $singletons = [
        TypeRepositoryInterface::class => TypeRepository::class,
        StyleRepositoryInterface::class => StyleRepository::class,
        MoveRepositoryInterface::class => MoveRepository::class,
    ];

    /**
     * Additional service providers.
     *
     * @type array|null
     */
    protected $providers = [
        //\ExamplePackage\Provider\ExampleProvider::class
    ];

    /**
     * The addon view overrides.
     *
     * @type array|null
     */
    protected $overrides = [
        //'streams::errors/404' => 'module::errors/404',
        //'streams::errors/500' => 'module::errors/500',
    ];

    /**
     * The addon mobile-only view overrides.
     *
     * @type array|null
     */
    protected $mobile = [
        //'streams::errors/404' => 'module::mobile/errors/404',
        //'streams::errors/500' => 'module::mobile/errors/500',
    ];

    /**
     * Register the addon.
     */
    public function register()
    {
        // Run extra pre-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Boot the addon.
     */
    public function boot()
    {
        // Run extra post-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Map additional addon routes.
     *
     * @param Router $router
     */
    public function map(Router $router)
    {
        // Register dynamic routes here for example.
        // Use method injection or commands to bring in services.
    }

}
