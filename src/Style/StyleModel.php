<?php namespace Finnito\MovesLibraryModule\Style;

use Finnito\MovesLibraryModule\Style\Contract\StyleInterface;
use Finnito\MovesLibraryModule\Move\MoveModel;
use Anomaly\Streams\Platform\Model\MovesLibrary\MovesLibraryStylesEntryModel;

class StyleModel extends MovesLibraryStylesEntryModel implements StyleInterface
{
    public function moves()
    {
        return $this->hasMany(MoveModel::class, "style_id")->get();
    }
}
