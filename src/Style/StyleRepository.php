<?php namespace Finnito\MovesLibraryModule\Style;

use Finnito\MovesLibraryModule\Style\Contract\StyleRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class StyleRepository extends EntryRepository implements StyleRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var StyleModel
     */
    protected $model;

    /**
     * Create a new StyleRepository instance.
     *
     * @param StyleModel $model
     */
    public function __construct(StyleModel $model)
    {
        $this->model = $model;
    }
}
