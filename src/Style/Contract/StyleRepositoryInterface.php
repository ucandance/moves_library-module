<?php namespace Finnito\MovesLibraryModule\Style\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface StyleRepositoryInterface extends EntryRepositoryInterface
{

}
