<?php namespace Finnito\MovesLibraryModule\Type;

use Finnito\MovesLibraryModule\Type\Contract\TypeInterface;
use Finnito\MovesLibraryModule\Move\MoveModel;
use Anomaly\Streams\Platform\Model\MovesLibrary\MovesLibraryTypeEntryModel;

class TypeModel extends MovesLibraryTypeEntryModel implements TypeInterface
{
    public function moves()
    {
        return $this->hasMany(MoveModel::class, "level_id")->get();
    }
}
