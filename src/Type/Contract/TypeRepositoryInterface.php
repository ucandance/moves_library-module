<?php namespace Finnito\MovesLibraryModule\Type\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface TypeRepositoryInterface extends EntryRepositoryInterface
{

}
