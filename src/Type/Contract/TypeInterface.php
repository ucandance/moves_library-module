<?php namespace Finnito\MovesLibraryModule\Type\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

interface TypeInterface extends EntryInterface
{

}
