<?php namespace Finnito\MovesLibraryModule;

use Anomaly\Streams\Platform\Addon\Module\Module;

class MovesLibraryModule extends Module
{

    /**
     * The navigation display flag.
     *
     * @var bool
     */
    protected $navigation = true;

    /**
     * The addon icon.
     *
     * @var string
     */
    protected $icon = 'fa fa-video-camera';

    /**
     * The module sections.
     *
     * @var array
     */
    protected $sections = [
        'moves' => [
            'buttons' => [
                'new_move',
            ],
        ],
        'styles' => [
            'buttons' => [
                'new_style',
            ],
        ],
        'type' => [
            'buttons' => [
                'new_type',
            ],
        ],
    ];

}
