<?php

return [
    'moves' => [
        'read',
        'write',
        'delete',
    ],
    'styles' => [
        'read',
        'write',
        'delete',
    ],
    'type' => [
        'read',
        'write',
        'delete',
    ],
];
