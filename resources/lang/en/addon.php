<?php

return [
    'title'       => 'Moves Library',
    'name'        => 'Moves Library Module',
    'description' => 'A library (or bible) of moves taught at UCanDance and related useful information.'
];
