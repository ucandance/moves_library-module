<?php

return [
    "name" => [
        "name" => "Name",
        "instructions" => "",
    ],
    "slug" => [
        "name" => "Slug",
        "instructions" => "Automatically generated - don't change me!",
    ],
    "style" => [
        "name" => "Dance Style",
        "instructions" => "",
    ],
    "level" => [
        "name" => "Level",
        "instructions" => "Beginner, Intermediate, Advance, Dip etc.",
    ],
    "video" => [
        "name" => "YouTube Video URL",
        "instructions" => "The whole URL",
    ],
    "counts" => [
        "name" => "Counts",
        "instructions" => "How many counts does the move take?",
    ],
    "description" => [
        "name" => "Description",
        "instructions" => "Put any further information about the move here.",
    ],
];
