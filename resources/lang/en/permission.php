<?php

return [
    'moves' => [
        'name'   => 'Moves',
        'option' => [
            'read'   => 'Can read moves?',
            'write'  => 'Can create/edit moves?',
            'delete' => 'Can delete moves?',
        ],
    ],
    'styles' => [
        'name'   => 'Styles',
        'option' => [
            'read'   => 'Can read styles?',
            'write'  => 'Can create/edit styles?',
            'delete' => 'Can delete styles?',
        ],
    ],
    'type' => [
        'name'   => 'Type',
        'option' => [
            'read'   => 'Can read type?',
            'write'  => 'Can create/edit type?',
            'delete' => 'Can delete type?',
        ],
    ],
];
